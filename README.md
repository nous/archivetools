Artix Linux Archive Tools
========================

Introduction
------------
**archivetools** is the project used to run the [Artix Linux Archive](https://archive.artixlinux.org). It's a turnkey solution to snapshot [Artix Linux](https://artixlinux.org) packages repositories and ISO images. You can deploy one for your own needs. Instructions on the ways it can me used are found at the [Arch Linux Wiki](https://wiki.archlinux.org/index.php/Arch_Linux_Archive).

The **Archive** is built by rsync'ing [mirror1.artixlinux.org](rsync://mirror1.artixlinux.org), or any other mirror, each day. *Rsync* features are used to transfer only the diff of new data from the previous snapshot and files are stored once with use of hardlinks.

Installation
------------
Create a pacman package and install it.

```
cd archivetools
makepkg -i
crontab -e -u archive	# add the following line
11 23 * * *	archive
```

Debug
-----
```
cd archivetools
export DEBUG=1
export ARCHIVE_CONFIG=archive.conf.test
./archive.sh
```

Dependencies
------------
- [Bash](http://www.gnu.org/software/bash/bash.html)
- [Rsync](http://rsync.samba.org/)
- [Hardlink](http://jak-linux.org/projects/hardlink/)
- [xz](http://tukaani.org/xz/)
- [util-linux](https://www.kernel.org/pub/linux/utils/util-linux/)

Sources
-------
**archivetools** sources are available on [github](https://github.com/seblu/archivetools/).
Artix-adapted sources at [Artix gitea](https://https://gitea.artixlinux.org/nous/archivetools).

License
-------
**archivetools** is licensied under the term of [GPL v2](http://www.gnu.org/licenses/gpl-2.0.html).

Author
------
**archivetools** was started by *Sébastien Luttringer* in August 2013 to replace the former *Arch Linux Rollback Machine* service.
